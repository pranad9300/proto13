export interface User {
  approved_by: number;
  created_by: number;
  creation_timestamp: string;
  dealer_installer_name: string;
  department: string;
  email: string;
  expiry_timestamp: string;
  first_name: string;
  full_name: string;
  has_password: boolean;
  id: number;
  is_active: boolean;
  is_confirmed: boolean;
  last_name: string;
  last_update_timestamp: string;
  last_updated_by: number;
  password_change_timestamp: string;
  user_type: string;
  xci_contact_email: string;
  xci_contact_first_name: string;
  xci_contact_last_name: string;
  xci_sf_id: string;
}
