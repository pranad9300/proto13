export interface Map {
  id: number;
  cartodb_id: string;
  last_sync_timestamp: string;
  layer_name: string;
  map_name: string;
}
