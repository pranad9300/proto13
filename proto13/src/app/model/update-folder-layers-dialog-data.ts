import {LayerFolder} from './layer-folder';
import {Map} from './map';

export interface UpdateFolderLayersDialogData {
  layers: Map[];
  folder: LayerFolder;
}
