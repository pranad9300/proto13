export interface CartoDbMapLayer {
  folder: any[];
  maps: any[];
  showSubOptions: boolean;
}
