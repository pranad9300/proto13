import {Group} from './group';
import {LayerFolder} from './layer-folder';

export interface UpdateGroupListDialogData {
  groups: Group[];
  folder: LayerFolder;
}
