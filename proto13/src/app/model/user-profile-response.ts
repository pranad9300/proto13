import {Group} from './group';
import {User} from './user';

export interface UserProfileResponse {
  created_by_email: string;
  groups: Group[];
  last_updated_by_email: string;
  user: User;
}
