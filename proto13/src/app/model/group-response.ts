import {Group} from './group';
import {LayerFolder} from './layer-folder';

export interface GroupResponse {
  group: Group;
  layer_folders: LayerFolder[];
}
