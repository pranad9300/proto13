import {Map} from './map';

export interface MapsListResponse {
  count: number;
  maps: Map[];
}
