import {LayerFolder} from './layer-folder';

export interface LayerFolderListResponse {
  count: number;
  layer_folders: LayerFolder[];
}
