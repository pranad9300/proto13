export interface Group {
  created_by: number;
  creation_timestamp: string;
  group_description: string;
  group_name: string;
  id: number;
}
