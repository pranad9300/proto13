export interface LayerFolder {
  layers: any[];
  id: number;
  created_by: number;
  folder_order: number;
  creation_timestamp: string;
  folder_description: string;
  folder_name: string;
}
