import {LayerFolder} from './layer-folder';

export interface FolderDialogData {
  create: boolean;
  folder: LayerFolder;
}
