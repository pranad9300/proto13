export interface Menu {
  icon: string;
  title: string;
  link: string;
  sub_menus: Menu[];
  expanded: boolean;
}


