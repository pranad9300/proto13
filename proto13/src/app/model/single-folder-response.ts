import {LayerFolder} from './layer-folder';

export interface SingleFolderResponse {
  folder: LayerFolder;
  cartodb_maps: any[];
}
