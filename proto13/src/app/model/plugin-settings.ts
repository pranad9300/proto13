export class PluginSettings {
  preSelectedAddress: string;
  latitude: string;
  longitude: string;
  userName: string;
  isSandbox: string;
  locale: string;
  preSelectedStreet: string;
  preSelectedCity: string;
  preSelectedState: string;
  preSelectedCountry: string;
  preSelectedPostalCode: string;
  stateList: any[];
}
