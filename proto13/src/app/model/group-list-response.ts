import {Group} from './group';

export interface GroupListResponse {
  count: number;
  groups: Group[];
}
