export class RouteConstants {
  public static CONSOLE = 'console';
  public static PAGES = 'pages/';
  public static LOGIN = 'login';
  public static HOME: any = 'home';
  public static USERS_LIST: any = RouteConstants.CONSOLE + '/users/list/';
  public static DEFAULT_USERS_LIST: any = RouteConstants.CONSOLE + '/users/list/-1';
  public static USERS_CREATE: any = RouteConstants.CONSOLE + '/users/create';
  public static USERS_BATCH_UPLOAD: any = RouteConstants.CONSOLE + '/users/batch-upload';
  public static GROUPS_LIST: any = RouteConstants.CONSOLE + '/groups/list';
  public static GROUPS_CREATE: any = RouteConstants.CONSOLE + '/groups/create';
  public static GROUPS_EDIT: any = RouteConstants.CONSOLE + '/groups/edit';
  public static LAYERS_LIST: any = RouteConstants.CONSOLE + '/layers/list';
  public static LAYERS_FOLDERS: any = RouteConstants.CONSOLE + '/layers/folders';
  public static RESET_PASSWORD: any = RouteConstants.PAGES + 'reset-password';
  public static SUPPORT = RouteConstants.PAGES + 'support';
  public static PROFILE: any = RouteConstants.PAGES + RouteConstants.CONSOLE + '/users/profile/' ;
  public static MAP: any = RouteConstants.PAGES + 'map';
}
