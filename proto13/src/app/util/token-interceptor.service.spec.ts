import {TestBed} from '@angular/core/testing';

import {TokenInterceptorService} from './token-interceptor.service';
import {AppModule} from '../app.module';

describe('TokenInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppModule]
  }));

  it('should be created', () => {
    const service: TokenInterceptorService = TestBed.get(TokenInterceptorService);
    expect(service).toBeTruthy();
  });
});
