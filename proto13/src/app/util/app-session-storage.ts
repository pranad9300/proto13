import {SessionConstants} from './session-constants';

export class AppSessionStorage {

  static setToken(token) {
    sessionStorage.setItem(SessionConstants.SESSION_TOKEN, token);
  }

  static getToken() {
    return sessionStorage.getItem(SessionConstants.SESSION_TOKEN);
  }

  static setUser(user: any) {
    sessionStorage.setItem(SessionConstants.SESSION_USER, JSON.stringify(user));
  }

  static getUser() {
    return JSON.parse(sessionStorage.getItem(SessionConstants.SESSION_USER));
  }

  static setIsAdmin(isAdmin: any) {
    sessionStorage.setItem(SessionConstants.SESSION_IS_ADMIN, isAdmin);
  }

  static getIsAdmin() {
    return sessionStorage.getItem(SessionConstants.SESSION_IS_ADMIN);
  }

  static setLayersData(layersData: any) {
    sessionStorage.setItem(SessionConstants.SESSION_LAYERS_DATA, layersData);
  }

  static getLayersData() {
    return sessionStorage.getItem(SessionConstants.SESSION_LAYERS_DATA);
  }

  static logoutUser() {
    sessionStorage.clear();
  }
}
