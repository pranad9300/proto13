export class SessionConstants {
  public static SESSION_TOKEN = 'token';
  public static SESSION_USER = 'user';
  public static SESSION_IS_ADMIN = 'is_admin';
  public static SESSION_LAYERS_DATA = 'layers_data';
}
