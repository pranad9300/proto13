export class UrlConstants {
  static URL_BASE = 'https://xplornetcoverage.ca:5000/api/v1.0/';
  // static URL_BASE = 'https://xplornetcoverage.ca:8000/api/v1.0/';
  public static URL_USERS = UrlConstants.URL_BASE + 'user/';
  public static URL_USER_PASSWORD_REQUEST = UrlConstants.URL_BASE + 'user/password_request';
  public static URL_GROUP = UrlConstants.URL_BASE + 'group/';
  public static URL_TOKEN = UrlConstants.URL_BASE + 'token';
  public static URL_CREATE_USERS = UrlConstants.URL_USERS + 'create';
  public static URL_LOAD_LAYERS = UrlConstants.URL_BASE + 'cartodb/maps/';
  public static URL_MAP_SYNC = UrlConstants.URL_BASE + 'cartodb/map_sync';
  public static URL_CARTODB_MAPS = UrlConstants.URL_BASE + 'cartodb/cartodb_map/';
  public static URL_LAYER_FOLDER = UrlConstants.URL_BASE + 'cartodb/layer_folder/';
  public static URL_CARTODB_MAP_FOLDER = UrlConstants.URL_BASE + 'cartodb/cartodb_map/folder/';
  public static URL_CARTODB_FOLDER_MAP_FOLDER = UrlConstants.URL_BASE + 'cartodb/folder_map/folder/';
  public static URL_CARTODB_FOLDER_MAP = UrlConstants.URL_BASE + 'cartodb/folder_map/';
  public static URL_GET_BULK_USERS = UrlConstants.URL_BASE + 'bulk/user';
  public static URL_UPLOAD_BULK_USERS = UrlConstants.URL_BASE + 'bulk/user';
  public static URL_USER_GROUP_MAP = UrlConstants.URL_BASE + 'group_map/';
  public static URL_USER_EXISTS = UrlConstants.URL_USERS + 'exists';
  public static URL_COVERAGE_INFO = UrlConstants.URL_BASE + 'cartodb/query/coverage_info';
}

