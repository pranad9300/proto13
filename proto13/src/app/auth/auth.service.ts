import { Injectable } from '@angular/core';
import { AppSessionStorage } from '../util/app-session-storage';
import { Router } from '@angular/router';
import { UrlConstants } from '../util/url-constants';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../model/user';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  token: string|null|undefined;
  user: User;
  isAdmin: boolean;

  constructor(private router: Router, private http: HttpClient) {}

  /*
   * @method loginUser
   * Retrieve user token
   */
  loginUser(credentials: any) {
    return this.http.get(UrlConstants.URL_TOKEN, {
      headers: new HttpHeaders().set(
        'Authorization',
        'Basic ' +
          window.btoa(
            decodeURIComponent(
              encodeURIComponent(credentials.email + ':' + credentials.password)
            )
          )
      ),
    });
  }

  

  getToken() {
    if (this.token === undefined || this.token === '') {
      this.token = AppSessionStorage.getToken();
    }
    return this.token;
  }

  setToken(token: string) {
    this.token = token;
    AppSessionStorage.setToken(token);
  }

  logoutUser() {
    this.token = undefined;
    AppSessionStorage.logoutUser();
  }

  getUser() {
    if (this.user === undefined) {
      this.user = AppSessionStorage.getUser();
    }
    return this.user;
  }

  setUser(user: User) {
    this.user = user;
    AppSessionStorage.setUser(user);
  }

  updateMyProfile(
    userId: number,
    data: { last_name: string; first_name: string }
  ) {
    return this.http
      .put(UrlConstants.URL_USERS + userId, data)
      .pipe(map((res: any) => res));
  }

  retrievePassword(email: string) {
    return this.http.get(UrlConstants.URL_USER_PASSWORD_REQUEST, {
      params: new HttpParams().set('email', email),
    });
  }

  setIsAdmin(isAdmin: boolean) {
    this.isAdmin = isAdmin;
    console.log('Setting is admin', isAdmin);
  }

  getIsAdmin() {
    if (this.isAdmin === undefined) {
      const stringIsAdmin = AppSessionStorage.getIsAdmin();
      this.isAdmin = stringIsAdmin === 'true' || stringIsAdmin === 'True';
    }
    return this.isAdmin;
  }
}
