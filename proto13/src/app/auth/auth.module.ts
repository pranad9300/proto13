import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ResetPasswordLinkComponent } from './reset-password-link/reset-password-link.component';
import { HeaderComponent } from './login/header/header.component';
import { LoginModalComponent } from './login/login-modal/login-modal.component';



@NgModule({
  declarations: [
    LoginComponent,
    ResetPasswordLinkComponent,
    HeaderComponent,
    LoginModalComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AuthModule { }
