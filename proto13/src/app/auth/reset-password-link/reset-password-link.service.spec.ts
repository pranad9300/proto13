import { TestBed } from '@angular/core/testing';

import { ResetPasswordLinkService } from './reset-password-link.service';

describe('ResetPasswordLinkService', () => {
  let service: ResetPasswordLinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResetPasswordLinkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
