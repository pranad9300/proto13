import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { LayoutComponent } from './layout/layout.component';
import { MenuComponent } from './menu/menu.component';
import { GroupSearchFilterPipe } from './filters/group-search-filter.pipe';
import { LayerSearchFilterPipe } from './filters/layer-search-filter.pipe';



@NgModule({
  declarations: [
    ConfirmationComponent,
    LayoutComponent,
    MenuComponent,
    GroupSearchFilterPipe,
    LayerSearchFilterPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
