import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { PagesRoutingModule } from './pages-routing.module';
import { MapComponent } from './map/map.component';
import { CartoDbLayerFilterPipe } from './map/carto-db-layer-filter.pipe';
import { CoverageElementComponent } from './map/coverage-element/coverage-element.component';
import { SalesForceErrorComponent } from './map/sales-force-error/sales-force-error.component';
import { SaveAddressModalComponent } from './map/save-address-modal/save-address-modal.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SupportComponent } from './support/support.component';



@NgModule({
  declarations: [
    PagesComponent,
    HomeComponent,
    MapComponent,
    CartoDbLayerFilterPipe,
    CoverageElementComponent,
    SalesForceErrorComponent,
    SaveAddressModalComponent,
    MyProfileComponent,
    ResetPasswordComponent,
    SupportComponent,
    
  ],
  imports: [
    CommonModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
