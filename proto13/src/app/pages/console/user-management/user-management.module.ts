import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserManagementRoutingModule } from './user-management-routing.module';
import { UserManagementComponent } from './user-management.component';
import { UsersListComponent } from './users-list/users-list.component';
import { BatchUploadComponent } from './batch-upload/batch-upload.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UserProfileComponent } from './user-profile/user-profile.component';


@NgModule({
  declarations: [
    UserManagementComponent,
    UsersListComponent,
    BatchUploadComponent,
    CreateUserComponent,
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    UserManagementRoutingModule
  ]
})
export class UserManagementModule { }
