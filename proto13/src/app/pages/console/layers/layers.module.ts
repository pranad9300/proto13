import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FoldersComponent } from './folders/folders.component';
import { SingleFolderComponent } from './folders/single-folder/single-folder.component';
import { UpdateFolderComponent } from './folders/update-folder/update-folder.component';
import { UpdateGroupsInFolderComponent } from './folders/update-groups-in-folder/update-groups-in-folder.component';
import { UpdateLayersInFolderComponent } from './folders/update-layers-in-folder/update-layers-in-folder.component';
import { LayerslistComponent } from './layerslist/layerslist.component';
import { LayersManagementComponent } from './layers-management.component';



@NgModule({
  declarations: [
    FoldersComponent,
    SingleFolderComponent,
    UpdateFolderComponent,
    UpdateGroupsInFolderComponent,
    UpdateLayersInFolderComponent,
    LayerslistComponent,
    LayersManagementComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LayersModule { }
