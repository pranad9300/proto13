import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayersManagementComponent } from './layers-management.component';

describe('LayersManagementComponent', () => {
  let component: LayersManagementComponent;
  let fixture: ComponentFixture<LayersManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayersManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayersManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
