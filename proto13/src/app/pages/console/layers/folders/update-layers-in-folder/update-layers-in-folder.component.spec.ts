import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLayersInFolderComponent } from './update-layers-in-folder.component';

describe('UpdateLayersInFolderComponent', () => {
  let component: UpdateLayersInFolderComponent;
  let fixture: ComponentFixture<UpdateLayersInFolderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateLayersInFolderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLayersInFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
