import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGroupsInFolderComponent } from './update-groups-in-folder.component';

describe('UpdateGroupsInFolderComponent', () => {
  let component: UpdateGroupsInFolderComponent;
  let fixture: ComponentFixture<UpdateGroupsInFolderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateGroupsInFolderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGroupsInFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
