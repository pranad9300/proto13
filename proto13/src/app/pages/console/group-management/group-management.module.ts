import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupManagementRoutingModule } from './group-management-routing.module';
import { CreateGroupComponent } from './create-group.component';
import { EditGroupComponent } from './edit-group/edit-group.component';
import { GroupListComponent } from './group-list/group-list.component';
import { GroupManagementComponent } from './group-management.component';


@NgModule({
  declarations: [
    CreateGroupComponent,
    EditGroupComponent,
    GroupListComponent,
    GroupManagementComponent
  ],
  imports: [
    CommonModule,
    GroupManagementRoutingModule
  ]
})
export class GroupManagementModule { }
