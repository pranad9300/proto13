import { TestBed } from '@angular/core/testing';

import { MapsLayersService } from './maps-layers.service';

describe('MapsLayersService', () => {
  let service: MapsLayersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapsLayersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
