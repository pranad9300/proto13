import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesForceErrorComponent } from './sales-force-error.component';

describe('SalesForceErrorComponent', () => {
  let component: SalesForceErrorComponent;
  let fixture: ComponentFixture<SalesForceErrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesForceErrorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesForceErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
