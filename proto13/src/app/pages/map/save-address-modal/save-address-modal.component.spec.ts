import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveAddressModalComponent } from './save-address-modal.component';

describe('SaveAddressModalComponent', () => {
  let component: SaveAddressModalComponent;
  let fixture: ComponentFixture<SaveAddressModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveAddressModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveAddressModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
