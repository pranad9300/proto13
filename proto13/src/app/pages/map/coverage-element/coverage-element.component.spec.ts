import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverageElementComponent } from './coverage-element.component';

describe('CoverageElementComponent', () => {
  let component: CoverageElementComponent;
  let fixture: ComponentFixture<CoverageElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoverageElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
