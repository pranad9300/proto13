import { TestBed } from '@angular/core/testing';

import { MapsLayersWebCallService } from './maps-layers-web-call.service';

describe('MapsLayersWebCallService', () => {
  let service: MapsLayersWebCallService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapsLayersWebCallService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
